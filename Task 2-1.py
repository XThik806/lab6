# Розробіть функції для здійснення наступних операцій зі списками:
# 1. Введення списку;
# 2. Заповнення списку випадковими числами;
# 3. Виведення списку;
# 4. Пошук елементу за значенням;
# 5. Пошук максимального елементу;
# 6. Сортування списку за зростанням (спаданням);
# 7. Пошук середнього арифметичного.

import myFunc

menu = 0

while (menu == 0):

    menu = int(input("Введіть число від 1 до 7, або 0 для виходу: "))

    if menu == 1: print(myFunc.list_input())

    n=4
    if menu == 2: print(myFunc.rand(n))

    if menu == 3: print(myFunc.print_list(myFunc.list_input()))

    value = 5
    if menu == 4: print(myFunc.find_element(myFunc.list_input(), value))

    if menu == 5: print(myFunc.maxElem(myFunc.list_input()))

    if menu == 6:
        print(myFunc.sort_list(myFunc.list_input()))

    if menu == 7: print(myFunc.average(myFunc.list_input()))