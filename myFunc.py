# Розробіть функції для здійснення наступних операцій зі списками:
# 1. Введення списку;
# 2. Заповнення списку випадковими числами;
# 3. Виведення списку;
# 4. Пошук елементу за значенням;
# 5. Пошук максимального елементу;
# 6. Сортування списку за зростанням (спаданням);
# 7. Пошук середнього арифметичного.

import random

def list_input():
    n = int(input("Введіть кількість елементів у списку: "))
    my_list = []
    for i in range(n):
        element = int(input(f"Введіть {i+1}-й елемент: "))
        my_list.append(element)
    return my_list

def rand(n):
    my_list = [random.randint(1, 100) for _ in range(n)]
    return my_list

def print_list(my_list):
    print("Список:", my_list)

def find_element(my_list, value):
    try:
        valueIndex = my_list.index(value)
        return f"Елемент {value} знаходиться на позиції {valueIndex}"
    except ValueError:
        return f"Елемент {value} не знайдено у списку"
    
def maxElem(my_list):
    return f"Максимальний елемент у списку: {max(my_list)}"

def sort_list(my_list):
    ascending = int(input("За зростанням чи спаданням? 1 або 0 відповідно: "))
    sorted_list = sorted(my_list) if ascending == 1 else sorted(my_list, reverse=True)
    return sorted_list

def average(my_list):
    avg = sum(my_list) / len(my_list)
    return f"Середнє арифметичне: {avg:.2f}"