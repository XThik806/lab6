# Задано список (якщо треба згенерувати відповідний список). Написати програму, в якій кожен
# елемент матриці помножено на максимальний елемент у поточному рядку.

def multiplyMatrix(matrix):

    result_matrix = []
    for row_index, row in enumerate(matrix):
        max_element = max(row)
        new_row = []
        for element in row:
            new_row.append(element * max_element)
        result_matrix.append(new_row)


    # max_elements = [max(row) for row in matrix]
    # result_matrix = [[element * max_elements[row_index] for element in row] for row_index, row in enumerate(matrix)]
    
    return result_matrix

matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [10, 11, 12]
]

print("Новий масив:")

newMatrix = multiplyMatrix(matrix)
# for row in enumerate(newMatrix):
#     print(row)

for row in newMatrix:
    print(*row, sep=', ')