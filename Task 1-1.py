# Задано список (якщо треба згенерувати відповідний список). Написати програму, яка 
# визначить суму елементів парних рядків матриці, записати результат у новий список

def sum_even_rows(matrix):
    result = []
    for row_index, row in enumerate(matrix):
        if row_index % 2 == 0:
            row_sum = sum(row)
            result.append(row_sum)
    return result

matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [10, 11, 12]
]

result_list = sum_even_rows(matrix)
print("Суми елементів парних рядків:", result_list)
